/// Determines whether the supplied string is a valid ISBN number
pub fn is_valid_isbn(isbn: &str) -> bool {
    
    let mut char_vec: Vec<char> = isbn.chars().collect(); // convert isbn string into a vector of characters
    let mut num_vec = Vec::new();
    let dash = '-';  // to be stripped out of the supplied isbn
    let mut sum = 0;  // hangs on to the total during the loop
    let mut counter = 10; // used for loop
    char_vec.retain(|&x| x != dash); // strip out the dashes if they are present

    if char_vec.len() == 10 && char_vec[9] != 'X'{
        // the length is correct and the last character is not an X
        for c in char_vec {
            // converts from a character vector to an int vector
            num_vec.push(c.to_digit(10).unwrap());
        } 
        for x in 0..10 {
            sum += num_vec[x] * counter as u32; // use the supplied formula to get the sum
            counter -= 1; 
        }
    } else if char_vec.len() == 10 && char_vec[9] == 'X' {
        // the length is correct and the last character is an X
        for n in 0..9 {
            num_vec.push(char_vec[n].to_digit(10).unwrap());
        }
        for x in 0..9 {
                sum += num_vec[x] * counter as u32; // use the supplied formula to get the sum
                counter -= 1; 
        }
        sum += 10 * 1; // accounting for the 'X' character
    } else { 
        // the length is not correct so this thing is not valid
        return false;
    }
    if sum % 11 == 0 {
        return true;
    } else {
        return false;
    }
}

fn main() {
    println!("{}", is_valid_isbn("3-598-21507-X"));
}
