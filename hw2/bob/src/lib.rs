pub fn reply(message: &str) -> &str {
    let response: &'static str;
    let upper = message.to_uppercase(); // to check for all caps
    let trimmed = message.trim_matches(|x| x == '\t' || x == ' ' || x == '\n'); // attempting to trim out all white space

    if message.contains("?") && message.contains("!") {
        // yelling a question
        println!("Calm down, I know what I'm doing!");
        response = "Calm down, I know what I'm doing!";
    } else if message.contains("?") {
        // asking a question
        println!("Sure.");
        response = "Sure.";
    } else if message.contains("!") || message == upper && !message.is_empty() {
        // either an exclamation point of all caps
        println!("Whoa, chill out!");
        response = "Whoa, chill out!";
    } else if trimmed.is_empty() {
        // checking it the string is empty
        println!("Fine. Be that way!");
        response = "Fine. Be that way!";
    } else {
        // any other input
        println!("Whatever.");
        response = "Whatever.";
    }
    response
}

fn main(){

    let question = String::from("Hey Bob can I come over??");
    let yell = String::from("You're annoying Bob I hate your stupid face!");
    let yell_question = String::from("What are you doing?!");
    let other = String::from("Tom-ay-to, tom-aaaah-to.");
    let silence = String::from("");

    println!("{}", question);
    reply(&question);


    println!("{}", yell);
    reply(&yell);

    println!("{}", yell_question);
    reply(&yell_question);

    println!("{}", other);
    reply(&other);

    println!("{}", silence);
    reply(&silence);

}
