pub fn raindrops(n: usize) -> String {
    
    let mut output = String::new();
    if n % 3 == 0 {
        output.push_str("Pling");
    }
    if n % 5 == 0 {
        output.push_str("Plang");
    }
    if n % 7 == 0 {
        output.push_str("Plong");
    }
    else if n % 3 != 0 && n % 5 != 0 && n % 7 != 0 {
        output = format!("{}", n)
    }
    return output;
}

fn main(){
    let num = 4;
    let mut output = raindrops(num);
    println!("{}", num);
    println!("{}", output);
}
