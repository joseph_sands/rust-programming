extern crate rand;
use rand::Rng;

pub struct Robot{
    name: String,
}

impl Robot {
    pub fn new() -> Robot {
        Robot{ name: generate_name() }
    }
    pub fn name<'a>(&'a self) -> &'a str {
        self.name.as_ref()
    }
    pub fn reset_name(&mut self) {
        self.name = generate_name();
    }
}

pub fn generate_name()-> String{
    let mut rng = rand::thread_rng();
    let first: char = rng.gen_range(b'A', b'Z') as char;
    let second: char = rng.gen_range(b'A', b'Z') as char;
    let number: u32 = rng.gen_range(0, 999);
    let robot_name = format!("{}{}{:03}",first,second,number);
    return robot_name;
}

fn main(){
    println!("Empty");
}
