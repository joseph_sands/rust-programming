use std::collections::HashMap; 
use std::str;

pub struct RNA {
    codon: HashMap < & 'static str, & 'static str >,
}

impl RNA {

    pub fn name_for(&self,input: &str) -> Result<&str, ()>{
        match self.codon.get(input) {
            Some(v)=>Ok(v),
            None => Err(()),
        }
    }

    pub fn of_rna(&self,input:&'static str) ->Result<Vec<&str>,()>{
        // split the input into chunks of size 3
        let split = input.as_bytes()
            .chunks(3)
            .map(|n| unsafe { str::from_utf8_unchecked(n) })
            .collect::<Vec<&str>>();

        let mut translate = Vec::new();
        for x in split {
            if self.codon.contains_key(x) && self.codon.get(x).unwrap().to_owned() == "stop codon".to_string(){ break; }
            if self.codon.contains_key(x){
                translate.push(self.codon.get(x).unwrap().to_owned());
            }
        }
        match translate.is_empty() {
            true=> Err(()),
            false=>Ok(translate),
        }
    }
}


pub fn parse(v: Vec<(&'static str, &'static str)>) -> RNA {
    let mut hash_map = HashMap::new();

    for (codon, name) in v {
        hash_map.insert(codon, name);
    }

    RNA { codon: hash_map }
}


fn main(){
    println!("Empty");
}