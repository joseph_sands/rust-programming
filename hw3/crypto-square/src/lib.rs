pub fn encrypt(input: &str) -> String {
    let string_vec: Vec<char> = input.to_lowercase().chars().filter(|x| x.is_alphabetic()).collect(); // lower case, no whitespace vector
    let final_str = String::new(); // will be the final string to be returned

    let length = string_vec.len();         // total length of the input string
    let sqr_root = (length as f64).sqrt(); // to be used to calculate r and c
    let r = sqr_root as usize;             // r does not depend on whether the input size is a perfect square
    let c = sqr_root.ceil() as usize;      // will work for both perfect and imperfect squares

    let mut square_code = Vec::new();  
    for x in 0..c {
        square_code.push(String::new());
        for y in 0..r {
            let index = (y * c + x) as usize;  // current row times total columns with column number offset
            if index < length {
                square_code[x].push(string_vec[index]);
            }
        }
    }
   
    let final_str = square_code.join(" ");     // space added to meet output needs
    return final_str;

}
    
fn main(){
    let test = "If man was meant to stay on the ground, god would have given us roots.";
    let answer = encrypt(test);
    println!("{}", answer);
}

