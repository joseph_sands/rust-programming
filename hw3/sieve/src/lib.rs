pub fn primes_up_to(upper_bound: u64) -> Vec<u64> {
   let mut primes = (2..upper_bound+1).collect::<Vec<u64>>(); // initial vector of all values from 2 to upper_bound
   let mut index = 0;
   while index != primes.len() {
       // loop over the entire vector, removing multiples of the current index
       let current = primes[index];
       primes.retain(|&x| x % current != 0 || x == current);
       index += 1;
   }
   return primes;
}

fn main(){
    // empty
}