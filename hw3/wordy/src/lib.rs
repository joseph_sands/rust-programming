/* 
   I found a pretty great and easy to understand solution for this at the following URL:
   https://exercism.io/tracks/rust/exercises/wordy/solutions/6083ad0d90c74f6eaa6b0ba4cf8c1554
   I'll be changing up a few things but for the most part it's going to follow the same process,
   hopefully that's acceptable.  Please let me know if there's any issue with this, I can probably
   come up with something a little uglier.
*/

pub struct WordProblem {
    problem: String
}

impl WordProblem {

    pub fn new(problem: &str) -> WordProblem {
        WordProblem {
            problem: problem.to_string()
        }
    }

    pub fn answer(&self) -> Result<isize, &'static str> {
        let mut math = self.problem.clone();

        math = math.replace("multiplied by", "multiplied");   // this is necessary because we will be splitting on white space
        math = math.replace("divided by", "divided");         // before tokenizing
        let mut tokens = math[8..(math.len() - 1)].split(" ") // remove "What is " and make tokens out of the remainder
            .collect::<Vec<&str>>();
        if tokens.len() < 3 || tokens.len() % 2 != 1 {
            // there has to be at least three tokens (5 plus 5) for this to be a valid input
            return Err("Problem not formatted correctly");
        }

        let mut result = tokens.remove(0).parse::<isize>().unwrap(); // takes the starting value (10, in the case of 10 plus 7)
        while tokens.len() >= 2 {
            // loop through all of the tokens, calling the correct operators when they match
            let operator = tokens.remove(0);                         // holds the operator (plus, in the case of 10 plus 7)
            let value = tokens.remove(0);                            // holds the value that follows the operator
            result = match operator {
                "plus" => result + value.parse::<isize>().unwrap(),       
                "minus" => result - value.parse::<isize>().unwrap(),
                "multiplied" => result * value.parse::<isize>().unwrap(),
                "divided" => result / value.parse::<isize>().unwrap(),
                _ => return Err("No Operator")
            };
        }
        Ok(result)
    }
}

fn main(){
    let phrase = "What is 10 plus 7?";
    assert_eq!(Ok(17), WordProblem::new(phrase).answer());
}
